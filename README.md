

### Tasks:

  1) Calculate probability lfv for 1mm of lead as a function of energy (50 - 100 GeV)  

### Install LHPDF

```
wget https://lhapdfsets.web.cern.ch/current/CT10nnlo.tar.gz
cp CT10nnlo.tar.gz /yourdir/pdfs
```

```
export LHAPDF="/yourdir/install"

export PATH=$PATH:${LHAPDF}/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${LHAPDF}/lib
export LHAPDF_DATA_PATH="/yourdir/pdfs"
```

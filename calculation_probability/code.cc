
#include "code.h"
// -----------------------------
// -----------------------------

int main(int argc, char *argv[])
{
    vector<string> quarks = {"u", "d", "s", "c", "b"};

    vector<string> vops = {"S", "V", "T"};
    vector<string> Sop = {"XY"};
    vector<string> Vop = {"LLRR", "LRRL"};
    vector<string> Top = {"LLRR"};

    double sigma = 0;

    int ilambda = 0;
    for (const auto &iop : vops)
    {

        vector<string> ivops;

        if (iop == "S")
            ivops = Sop;

        if (iop == "V")
            ivops = Vop;

        if (iop == "T")
            ivops = Top;

        double sigma_operator = 0;

        for (const auto &index : ivops)
        {

            cout << "   " << iop << " operators " << index << endl;
            for (const auto &iquark : quarks)
            {

                MyFunction f2;

                f2.SetTarget("Pb");    // Fe target
                f2.SetBeamEnergy(100); // GeV
                f2.Calculate_s();      // 2 * m_Np * E_lepton;
                f2.SetQuark(iquark);   //  d  , u ,  s ,  c ,  b

                string ioperator = iop + index;


                f2.SetOp(ioperator); // "S", "VLLRR", "VLRRL", "TLLRR"

                ROOT::Math::Functor wf(f2, 2);
                double a[2] = {0, 0};
                double b[2] = {1, 1};

                // ROOT::Math::IntegrationMultiDim::kVEGAS
                // ROOT::Math::IntegrationMultiDim::kADAPTIVE
                // ROOT::Math::IntegrationMultiDim::kPLAIN
                // ROOT::Math::IntegrationMultiDim::kMISER

                ROOT::Math::IntegratorMultiDim ig(ROOT::Math::IntegrationMultiDim::kADAPTIVE);
                ig.SetFunction(wf);
                // ig.SetRelTolerance(1e-02);
                // ig.SetAbsTolerance(1e-03);

                // ROOT::Math::IntegratorMultiDimOptions igopt = ig.Options();
                // igopt.SetNCalls(10000000);
                //  igopt.Print();

                double value_integral = ig.Integral(a, b);

                double QA = (f2.Get_s() / (64. * TMath::Pi())) * value_integral;

                std::cout << "QA" << iquark << "=" << QA << "  " << std::endl;

                double Lambda = 1000 * f2.Get_Lambda(ilambda); // TeV to GeV

                sigma += QA / pow(Lambda, 4);
                sigma_operator += sigma;
            }
        }
        ilambda++;

        cout << " sigma="<< sigma_operator <<" GeV^-2 "<< endl;
    }

    cout<<endl;
    double one_pb = 2.56819 * pow(10, -9);
    cout << "sigma=" << sigma << " GeV^-2 " << endl;
    cout << "sigma=" << sigma / one_pb << " pb " << endl;

    return 0;
}

// 1 pb        =  2.56819×10−9 GeV

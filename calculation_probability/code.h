

#include "Math/IntegratorMultiDim.h"
#include "Math/Functor.h"
#include "TMath.h"

#include "LHAPDF/LHAPDF.h"
#include <iostream>
#include <fstream>

using namespace LHAPDF;
using namespace std;

// =====================================
// =====================================
vector<double> LambdaFe_eBeam = {
    // "u", "d", "s", "c", "b"
    
    // S-operator
    0.23,
    0.24,
    0.15,
    0.11,
    0.05, 

    // V-operator
    // "LLRR"
    0.43,
    0.44,
    0.26,
    0.18,
    0.06,
    // "LRRL"
    0.35,
    0.36,
    0.26,
    0.18,
    0.06,

    // T-operator
    0.77,
    0.78,
    0.50,
    0.34,
    0.12
};

// =====================================
// =====================================
vector<double> LambdaPb_eBeam = {
    // "u", "d", "s", "c", "b"

    // S-operator
    0.18,
    0.19,
    0.12,
    0.09,
    0.04, 

    // V-operator
    // "LLRR"
    0.34,
    0.35,
    0.21,
    0.15,
    0.05,
    //"LRRL"
    0.28,
    0.29,
    0.21,
    0.15,
    0.05,

    // T-operator
    0.61,
    0.63,
    0.40,
    0.28,
    0.09 
};
// =====================================
// =====================================

// -------------------------------------
const string setname = "CT10nnlo";
const int imem = 1;
const PDF *pdf = mkPDF(setname, imem);

// -----------------------------
// d 1
// u 2
// s 3
// c 4
// b 5

class MyFunction
{
    const double m_Np = 0.9382726; // GeV

    string NameTarget;
    double Z;
    double A;
    double E_lepton;
    double s;
    string namequark;
    string op;

public:
    double operator()(const double *fx)
    {

        double x, y, Q2, uA, anti_uA;

        x = fx[0];
        y = fx[1];

        Q2 = s * x * y;

        if (namequark == "u") // u
        {
            uA = (Z * pdf->xfxQ2(2, x, Q2) + (A - Z) * pdf->xfxQ2(1, x, Q2));
            anti_uA = A * pdf->xfxQ2(-2, x, Q2);
        }
        else if (namequark == "d") // d
        {
            uA = (Z * pdf->xfxQ2(1, x, Q2) + (A - Z) * pdf->xfxQ2(2, x, Q2));
            anti_uA = A * pdf->xfxQ2(-1, x, Q2);
        }
        else if (namequark == "s") // s
        {
            uA = A * pdf->xfxQ2(3, x, Q2);
            anti_uA = uA;
        }
        else if (namequark == "c") // c
        {
            uA = A * pdf->xfxQ2(4, x, Q2);
            anti_uA = uA;
        }
        else if (namequark == "b") // b
        {
            uA = A * pdf->xfxQ2(5, x, Q2);
            anti_uA = uA;
        }
        else
        {
            cout << "not corrent quark" << endl;
            exit(0);
        }

        double xf = (Get_f(y) * uA + Get_g(y) * anti_uA);

        return xf;
    }

    void SetBeamEnergy(double fE_lepton)
    {

        E_lepton = fE_lepton;
    }

    void Calculate_s()
    {

        s = 2 * m_Np * E_lepton;
    }

    double Get_s()
    {

        return s;
    }

    void SetTarget(string fNameTarget)
    {

        NameTarget = fNameTarget;
        if (NameTarget == "Fe")
        {
            Z = 26;
            A = 56;
        }
        else if (NameTarget == "Pb")
        {
            Z = 82;
            A = 207;
        }
        else
        {

            exit(0);
        }
    }

    void SetQuark(string fnamequark)
    {
        namequark = fnamequark;
    }

    void SetOp(string fop)
    {
        op = fop;
    }

    double Get_f(double y)
    {

        if (op == "SXY")
        {

            return y * y;
        }
        if (op == "TLLRR")
        {
            return 16 * pow((2 - y), 2);
        }
        if (op == "VLLRR")
        {

            return 4;
        }
        if (op == "VLRRL")
        {

            return 4 * pow((1 - y), 2);
            ;
        }
        exit(0);
        return 0;
    }

    double Get_g(double y)
    {

        if (op == "SXY")
        {

            return y * y;
        }
        if (op == "TLLRR")
        {

            return 16 * pow((2 - y), 2);
        }

        if (op == "VLLRR")
        {

            return 4;
        }

        if (op == "VLRRL")
        {

            return 4 * pow((1 - y), 2);
            ;
        }

        exit(0);
        return 0;
    }

    double Get_Lambda(int index)
    {

        if (NameTarget == "Fe")
        {

            return LambdaFe_eBeam[index];
        }

        if (NameTarget == "Pb")
        {

            return LambdaPb_eBeam[index];
        }

        cout << "error in Get_Lambda" << endl;
        return 0;
    }
};

// ===========================
